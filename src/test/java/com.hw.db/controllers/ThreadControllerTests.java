package com.hw.db.controllers;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.*;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThreadControllerTests {
    private User user;
    private Forum forum;
    private Thread thread;
    private Post post;


    @BeforeEach
    @DisplayName("thread creation test")
    void createForumTest() {
        user = new User("author", "some@email.mu", "name", "nothing");
        forum = new Forum(12, "slug", 3, "forum", "some");
        thread = new Thread("author", new Timestamp(System.currentTimeMillis()), "forum",
                "message", "slug", "title", 13);
        thread.setId(1);
        post = new Post("author", new Timestamp(System.currentTimeMillis()), "forum", "message", 0, 1, false);
    }

    @Test
    @DisplayName("Correctly return thread by slug or ID")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(thread, controller.CheckIdOrSlug("1"));
            assertEquals(thread, controller.CheckIdOrSlug("slug"));
        }
    }

    @Test
    @DisplayName("Correctly creates a new post")
    void testCreatePost() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
             MockedStatic<ForumDAO> forumMock = Mockito.mockStatic(ForumDAO.class);
             MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class);
             MockedStatic<PostDAO> postMock = Mockito.mockStatic(PostDAO.class)) {

            userMock.when(() -> UserDAO.Search("author")).thenReturn(user);
            userMock.when(() -> UserDAO.Info("author")).thenReturn(user);
            forumMock.when(() -> ForumDAO.Info("slug")).thenReturn(forum);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();

            ArrayList<Post> posts = new ArrayList<>();
            posts.add(post);

            assertEquals(
                    ResponseEntity.status(HttpStatus.CREATED).body(posts),
                    controller.createPost("slug", posts)
            );
        }
    }

    @Test
    @DisplayName("Correctly return a thread posts")
    void testPosts() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
             MockedStatic<ForumDAO> forumMock = Mockito.mockStatic(ForumDAO.class);
             MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class);
             MockedStatic<PostDAO> postMock = Mockito.mockStatic(PostDAO.class)) {

            userMock.when(() -> UserDAO.Search("author")).thenReturn(user);
            userMock.when(() -> UserDAO.Info("author")).thenReturn(user);
            forumMock.when(() -> ForumDAO.Info("slug")).thenReturn(forum);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();

            ArrayList<Post> posts = new ArrayList<>();
            posts.add(post);

            threadMock
                    .when(() -> ThreadDAO.getPosts(1, 10, 0, "flat", false))
                    .thenReturn(posts);

            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(posts),
                    controller.Posts("slug", 10, 0, "flat", false)
            );
        }
    }

    @Test
    @DisplayName("Correctly changes a thread")
    void testChange() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
             MockedStatic<ForumDAO> forumMock = Mockito.mockStatic(ForumDAO.class);
             MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class);
             MockedStatic<PostDAO> postMock = Mockito.mockStatic(PostDAO.class)) {

            userMock.when(() -> UserDAO.Search("author")).thenReturn(user);
            userMock.when(() -> UserDAO.Info("author")).thenReturn(user);
            forumMock.when(() -> ForumDAO.Info("slug")).thenReturn(forum);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);

            threadController controller = new threadController();

            Thread newThread = new Thread("author", new Timestamp(System.currentTimeMillis()), "forum",
                    "NEW message", "slug", "title", 13);

            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(newThread);

            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(newThread),
                    controller.change("slug", newThread)
            );
        }
    }

    @Test
    @DisplayName("Correctly returns thread info")
    void testInfo() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
             MockedStatic<ForumDAO> forumMock = Mockito.mockStatic(ForumDAO.class);
             MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class);
             MockedStatic<PostDAO> postMock = Mockito.mockStatic(PostDAO.class)) {

            userMock.when(() -> UserDAO.Search("author")).thenReturn(user);
            userMock.when(() -> UserDAO.Info("author")).thenReturn(user);
            forumMock.when(() -> ForumDAO.Info("slug")).thenReturn(forum);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);

            threadController controller = new threadController();

            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(thread),
                    controller.info("slug")
            );
        }

    }

    @Test
    @DisplayName("Correctly creates a vote in a thread")
    void testCreateVote() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class);
             MockedStatic<ForumDAO> forumMock = Mockito.mockStatic(ForumDAO.class);
             MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class);
             MockedStatic<PostDAO> postMock = Mockito.mockStatic(PostDAO.class)) {

            userMock.when(() -> UserDAO.Search("author")).thenReturn(user);
            userMock.when(() -> UserDAO.Info("author")).thenReturn(user);
            forumMock.when(() -> ForumDAO.Info("slug")).thenReturn(forum);
            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug")).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadById(1)).thenReturn(thread);

            threadController controller = new threadController();

            Thread newThread = new Thread("author", new Timestamp(System.currentTimeMillis()), "forum",
                    "NEW message", "slug", "title", 14);

            Vote vote = new Vote("author", 1);

            assertEquals(
                    ResponseEntity.status(HttpStatus.OK).body(thread),
                    controller.createVote("slug", vote)
            );
        }

    }
}
